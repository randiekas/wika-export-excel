from flask import Flask, url_for, jsonify
from markupsafe import escape
from flask import request
from flask import send_file
from flask_cors import CORS
from jadwal_rinci_internal import exportJadwalRinciInternal
from jadwal_rinci_smk3l import exportJadwalRinciSMK3l
from hasil_temuan_smk3l import exportHasilTemuanSMK3l
from hasil_temuan_internal import exportHasilTemuanInternal
from perbaikan_internal import exportPerbaikanInternal
from perbaikan_external import exportPerbaikanExternal
from evaluasi_internal import exportEvaluasiInternal
import requests
app = Flask(__name__)
CORS(app)

@app.route('/')
def index():
    return 'index'

@app.route('/login')
def login():
    return 'login'

@app.route('/user/<username>')
def profile(username):
    return '{}\'s profile'.format(escape(username))

@app.route('/export-audit', methods=['POST', 'GET'])
def exportAudit():
    path = "./audit.xlsx"
    return send_file(path, as_attachment=True)

@app.route('/export-jadwal-rinci-internal', methods=['POST', 'GET'])
def jadwalRinciInternal():
    post = request.json
    path = exportJadwalRinciInternal(post)
    return send_file(path, as_attachment=True)

@app.route('/export-jadwal-rinci-smk3l', methods=['POST', 'GET'])
def jadwalRinciSMK3l():
    post = request.json
    path = exportJadwalRinciSMK3l(post)
    return send_file(path, as_attachment=True)

@app.route('/export-hasil-temuan-smk3l', methods=['POST', 'GET'])
def hasilTemuanSMK3l():
    post = request.json
    path = exportHasilTemuanSMK3l(post)
    return send_file(path, as_attachment=True)

@app.route('/export-hasil-temuan-internal', methods=['POST', 'GET'])
def hasilTemuanInternal():
    post = request.json
    path = exportHasilTemuanInternal(post)
    return send_file(path, as_attachment=True)
    
@app.route('/export-perbaikan-internal', methods=['POST', 'GET'])
def perbaikanInternal():
    post = request.json
    path = exportPerbaikanInternal(post)
    return send_file(path, as_attachment=True)

@app.route('/export-perbaikan-external', methods=['POST', 'GET'])
def perbaikanExternal():
    post = request.json
    path = exportPerbaikanExternal(post)
    return send_file(path, as_attachment=True)

@app.route('/export-evaluasi-internal', methods=['POST', 'GET'])
def evaluasiInternal():
    post = request.json
    path = exportEvaluasiInternal(post)
    return send_file(path, as_attachment=True)

@app.route('/tes', methods=['POST', 'GET'])
def tes():
    
    headers = {
        'Connection': 'keep-alive',
        'Accept': 'application/json, text/plain, */*',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXIiOnsicGFzc3dvcmQiOiIkMmEkMTAkRi5xcmJTVUpzemMvN2l3aU85VkxtT0wyTFlZUnRySmh1Wm15VEg5T2lCa2x2TnJQbm1ITnUiLCJ1c2VyIjoiZGV2ZWxvcGVyIiwiaWQiOjF9LCJpZCI6MTMzfSwiaWF0IjoxNjA0NjY1Njc0fQ.B7cjXgbK5H8kCpLJhNpVqm1m7Ft0gjQLw3Hn6-1dEkU',
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36',
        'Content-Type': 'application/json',
        'Origin': 'http://localhost:4200',
        'Sec-Fetch-Site': 'cross-site',
        'Sec-Fetch-Mode': 'cors',
        'Sec-Fetch-Dest': 'empty',
        'Referer': 'http://localhost:4200/',
        'Accept-Language': 'en-US,en;q=0.9,id;q=0.8',
    }

    data = '{"where":{"deactivate":false,"jadwalRinci":224}}'
    response = requests.post('https://iams.weare.co.id:3000/hasil-temuan/internals', headers=headers, data=data)

    return response.content

from openpyxl import load_workbook
def exportPerbaikanInternal(post):
    wb = load_workbook(filename = 'template/perbaikan_internal.xlsx')
    export_name = "template/perbaikan_internal_edit.xlsx"
    sheet_ranges = wb['Sheet1']
    sheet_ranges['H8'] = "Tanggal Audit          : "+post["waktu"]
    
    
    for x in post["proses"]:
        sheet_ranges.insert_rows(14)
    no = 0
    for item in post["proses"]:
        ke = str(no+14)
        sheet_ranges["A"+ke] = no+1
        sheet_ranges["B"+ke] = item["no_ptkp"]
        sheet_ranges['C'+ke] = item["temuan"]
        sheet_ranges['D'+ke] = item["kategori_sistem"]
        sheet_ranges['E'+ke] = item["kategori_temuan"]
        sheet_ranges['F'+ke] = item["analisa"]
        sheet_ranges['G'+ke] = item["rencana_perbaikan"]
        sheet_ranges['H'+ke] = item["waktu"]
        sheet_ranges['I'+ke] = item["oleh"]
        sheet_ranges['J'+ke] = item["open_close"]
        no+=1
    
    sheet_ranges.delete_rows(13)
    wb.save(export_name)
    

    return export_name

# post={}
# post["unit_kerja"]      = "Unit Kerja"
# post["alamat"]          = "Alamat"
# post["surat_tugas"]     = "Surat Tugas"
# post["lead_auditor"]    = "Lead Auditor"
# post["auditor"]         = "Auditor"
# post["auditee"]         = "Auditee"

# exportJadwalRinciInternal(post)
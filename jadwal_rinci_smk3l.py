from openpyxl import load_workbook
def exportJadwalRinciSMK3l(post):
    wb = load_workbook(filename = 'template/jadwal_rinci_smk3l.xlsx')
    export_name = "template/jadwal_rinci_smk3l_edit.xlsx"
    sheet_ranges = wb['Sheet1']
    sheet_ranges['B5'] = "Unit Kerja   : "+post["unit_kerja"]
    sheet_ranges['B6'] = "Jadwal Audit   : "+post["waktu"]
    sheet_ranges['E4'] = "Surat Tugas : "+post["surat_tugas"]
    sheet_ranges['E5'] = "AUDITEE : "+post["auditee"]
    sheet_ranges['E6'] = "Lead Auditor : "+post["lead_auditor"]
    # sheet_ranges['C5'] = "Unit Kerja : Biro "
    # sheet_ranges['C6'] = "Alamat : Jl. didjslkdjf"
    # sheet_ranges['D5'] = "Surat Tugas : ST.02"
    # sheet_ranges['D6'] = "Lead Auditor : Randi Kusdiyanta "
    # sheet_ranges['D10'] = "Auditor : Randi Anggraini "

    
    for x in post["proses"]:
        sheet_ranges.insert_rows(14)    
    no = 0
    for item in post["proses"]:
        ke = str(no+14)
        sheet_ranges["B"+ke] = no+1
        sheet_ranges["C"+ke] = item["nomor_kriteria"]
        sheet_ranges['D'+ke] = item["kriteria"]
        sheet_ranges['E'+ke] = item["contoh"]
        sheet_ranges['F'+ke] = item["interpretasi"]
        sheet_ranges['G'+ke] = item["mulai"]
        sheet_ranges['H'+ke] = item["selesai"]
        sheet_ranges['I'+ke] = item["auditor"]
        no+=1
    wb.save(export_name)
    return export_name

# post={}
# post["unit_kerja"]      = "Unit Kerja"
# post["alamat"]          = "Alamat"
# post["surat_tugas"]     = "Surat Tugas"
# post["lead_auditor"]    = "Lead Auditor"
# post["auditor"]         = "Auditor"
# post["auditee"]         = "Auditee"

# exportJadwalRinciInternal(post)
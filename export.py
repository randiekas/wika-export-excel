from openpyxl import load_workbook

wb = load_workbook(filename = 'audit.xlsx')
sheet_ranges = wb['Sheet1']
sheet_ranges['B3'] = "Unit Kerja : Tamansari Lagoon Manado"
sheet_ranges['F5'] = "01/PTKA/TSLN/26/03/2019"
sheet_ranges['F8'] = "Fungsi bagian"
sheet_ranges['F9'] = "08/07/2020"
sheet_ranges['K5'] = "Peserta Spot Chek"
sheet_ranges['K6'] = "Biro Bagian"
sheet_ranges['K7'] = "26 Maret 2019"

sheet_ranges['B12'] = "Untuk pekerjaaan Keamanan PT. Uphus Khamang Indonesia, Kontrak sudah dibuat oleh bagian Operasi, namun kontrak habis pertanggal 31 Desember 2018, dan sampai dengan 26 Maret 2019 belum dibuat Add kontraknya."
sheet_ranges['I12'] = "Untuk pekerjaaan Keamanan PT. Uphus Khamang Indonesia, Kontrak sudah dibuat oleh bagian Operasi, namun kontrak habis pertanggal 31 Desember 2018, dan sampai dengan 26 Maret 2019 belum dibuat Add kontraknya."
sheet_ranges['B14'] = "sehingga tidak ada dasar untuk pelaksanaan pekerjaan."
sheet_ranges['B15'] = "Referensi : WR-PPR-IK-204, REV.01 Amandemen 01. Pengadaan"
sheet_ranges['B18'] = "[   ] Digunakan sebagaimana adanya (use as is)"
sheet_ranges['B20'] = "[   ] Dikerjakan ulang (rework)"
sheet_ranges['B21'] = "[   ] Pemakaian alternatif (down grade)"
sheet_ranges['B22'] = "[   ] Ditolak / dibuang (scrap)"
sheet_ranges['B23'] = "[   ] Tidak diperlukan tindakan korektif (jelaskan sebabnya) :"
sheet_ranges['I18'] = "Perpanjang kontrak pekerjaan bersama PT. Uphus Khamang Indonesia"
sheet_ranges['I22'] = "Target Tgl Selesai : 01 April 2020"

sheet_ranges['D25'] = "Tamansari Lagoon"
sheet_ranges['D26'] = "Joseph A. Hutapea"
sheet_ranges['H25'] = "Kasmat Makruf"
sheet_ranges['H26'] = ""
sheet_ranges['H27'] = "Chief. Operation"
sheet_ranges['H27'] = "Chief. Operation"
sheet_ranges['C30'] = "kontrak kerja bersama PT. Uphus Khamang Indonesia sudah ada sehingga sudah adanya dasar pelaksanaan pekerjaan."

wb.save("audit_edit.xlsx")
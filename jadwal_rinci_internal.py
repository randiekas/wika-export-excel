from openpyxl import load_workbook
def exportJadwalRinciInternal(post):
    wb = load_workbook(filename = 'template/jadwal_rinci_internal.xlsx')
    export_name = "template/jadwal_rinci_internal_edit.xlsx"
    sheet_ranges = wb['Sheet1']
    sheet_ranges['C5'] = post["unit_kerja"]
    sheet_ranges['C6'] = post["alamat"]
    sheet_ranges['D5'] = "Surat Tugas : "+post["surat_tugas"]
    sheet_ranges['D6'] = "Lead Auditor : "+post["lead_auditor"]
    sheet_ranges['D10'] = "Auditor : "+post["auditor"]
    sheet_ranges['C10'] = "Waktu : "+post["waktu"]
    sheet_ranges['C12'] = "AUDITEE : "+post["auditee"]

    # sheet_ranges['C5'] = "Unit Kerja : Biro "
    # sheet_ranges['C6'] = "Alamat : Jl. didjslkdjf"
    # sheet_ranges['D5'] = "Surat Tugas : ST.02"
    # sheet_ranges['D6'] = "Lead Auditor : Randi Kusdiyanta "
    # sheet_ranges['D10'] = "Auditor : Randi Anggraini "

    
    for x in post["proses"]:
        sheet_ranges.insert_rows(15)    
    no = 0
    for item in post["proses"]:
        ke = str(no+15)
        sheet_ranges["A"+ke] = item["waktu"]
        sheet_ranges['B'+ke] = item["proses"]
        sheet_ranges['C'+ke] = item["referensi"]
        sheet_ranges['D'+ke] = item["fungsi"]
        sheet_ranges['E'+ke] = item["auditor"]
        no+=1
    wb.save(export_name)
    return export_name

# post={}
# post["unit_kerja"]      = "Unit Kerja"
# post["alamat"]          = "Alamat"
# post["surat_tugas"]     = "Surat Tugas"
# post["lead_auditor"]    = "Lead Auditor"
# post["auditor"]         = "Auditor"
# post["auditee"]         = "Auditee"

# exportJadwalRinciInternal(post)
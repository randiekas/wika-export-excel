from openpyxl import load_workbook
def exportHasilTemuanSMK3l(post):
    wb = load_workbook(filename = 'template/hasil_temuan_smk3l.xlsx')
    export_name = "template/hasil_temuan_smk3l_edit.xlsx"
    sheet_ranges = wb['Sheet1']
    sheet_ranges['D9'] = ": "+post["unit_kerja"]
    sheet_ranges['D10'] = ": "+post["alamat"]
    sheet_ranges['D11'] = ": "+post["waktu"]
    sheet_ranges['E8'] = "Surat Tugas : "+post["surat_tugas"]
    sheet_ranges['E9'] = "AUDITEE : "+post["auditee"]
    sheet_ranges['E10'] = "Lead Auditor : "+post["lead_auditor"]
    # sheet_ranges['C5'] = "Unit Kerja : Biro "
    # sheet_ranges['C6'] = "Alamat : Jl. didjslkdjf"
    # sheet_ranges['D5'] = "Surat Tugas : ST.02"
    # sheet_ranges['D6'] = "Lead Auditor : Randi Kusdiyanta "
    # sheet_ranges['D10'] = "Auditor : Randi Anggraini "

    
    for x in post["proses"]:
        sheet_ranges.insert_rows(25)    
    no = 0
    for item in post["proses"]:
        ke = str(no+25)
        sheet_ranges["B"+ke] = no+1
        sheet_ranges["C"+ke] = item["nomor_kriteria"]
        sheet_ranges['D'+ke] = item["kriteria"]
        sheet_ranges['E'+ke] = item["interpretasi"]
        sheet_ranges['F'+ke] = item["contoh"]
        sheet_ranges['G'+ke] = item["dilanggar"]
        sheet_ranges['H'+ke] = item["temuan"]
        sheet_ranges['I'+ke] = item["kesesuaian"]
        sheet_ranges['J'+ke] = item["kritikal"]
        sheet_ranges['K'+ke] = item["mayor"]
        sheet_ranges['L'+ke] = item["minor"]
        no+=1
    wb.save(export_name)
    return export_name

# post={}
# post["unit_kerja"]      = "Unit Kerja"
# post["alamat"]          = "Alamat"
# post["surat_tugas"]     = "Surat Tugas"
# post["lead_auditor"]    = "Lead Auditor"
# post["auditor"]         = "Auditor"
# post["auditee"]         = "Auditee"

# exportJadwalRinciInternal(post)
from openpyxl import load_workbook
def exportEvaluasiInternal(post):
    wb = load_workbook(filename = 'template/evaluasi_internal.xlsx')
    export_name         = "template/evaluasi_internal_edit.xlsx"
    sheet_ranges        = wb['Sheet1']
    no = 0

    pertanyaan = {}
    for item in post["pertanyaan"]:
        pertanyaan[item["id"]] = item["pertanyaan"]
    for item in post["proses"]:
        ke = str(no+5)
        sheet_ranges['B'+ke]  = "Nama Auditee: "+item["auditee"]
        ke = str(no+6)
        sheet_ranges['B'+ke]  = "Tgl Audit: "+post["waktu"]
        ke = str(no+7)
        sheet_ranges['B'+ke]  = "Nama Auditor: "+item["auditor"]
        ke = str(no+8)
        sheet_ranges['B'+ke]  = "Unit Yang di Audit: "+post["unit_kerja"]
        sheet_ranges['C'+ke]  = "Baik"
        sheet_ranges['D'+ke]  = ""
        sheet_ranges['E'+ke]  = "Cukup"
        sheet_ranges['F'+ke]  = ""
        sheet_ranges['G'+ke]  = "Kurang"
        sheet_ranges['F'+ke]  = ""

        for row in item["pertanyaan"]:
            ke = str(no+9)
            sheet_ranges["B"+ke] = pertanyaan[row["idKuisioner"]]
            sheet_ranges["C"+ke] = "V" if row["nilai"] == 1 else ''
            sheet_ranges['D'+ke] = "1"
            sheet_ranges['E'+ke] = "V" if row["nilai"] == 2 else ''
            sheet_ranges['F'+ke] = "2"
            sheet_ranges['G'+ke] = "V" if row["nilai"] == 3 else ''
            sheet_ranges['H'+ke] = "3"
            no+=1

        no+=no+9
    
    wb.save(export_name)
    

    return export_name

# post={}
# post["unit_kerja"]      = "Unit Kerja"
# post["alamat"]          = "Alamat"
# post["surat_tugas"]     = "Surat Tugas"
# post["lead_auditor"]    = "Lead Auditor"
# post["auditor"]         = "Auditor"
# post["auditee"]         = "Auditee"

# exportJadwalRinciInternal(post)
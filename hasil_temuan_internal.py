from openpyxl import load_workbook
def exportHasilTemuanInternal(post):
    wb = load_workbook(filename = 'template/hasil_temuan_internal.xlsx')
    export_name = "template/hasil_temuan_internal_edit.xlsx"
    sheet_ranges = wb['Sheet1']
    sheet_ranges['B8'] = "Unit Kerja : "+post["unit_kerja"]
    sheet_ranges['B9'] = "Diperiksa Tanggal : "+post["waktu"]
    
    
    for x in post["proses"]:
        sheet_ranges.insert_rows(12)
    no = 0
    for item in post["proses"]:
        ke = str(no+12)
        sheet_ranges["B"+ke] = no+1
        sheet_ranges["C"+ke] = item["proses"]
        sheet_ranges['D'+ke] = item["temuan"]
        sheet_ranges['H'+ke] = item["tinjauan"]
        no+=1
    
    ke = str(no+18)
    sheet_ranges['C'+ke] = post["auditee"]
    sheet_ranges['D'+ke] = post["lead_auditor"]
    sheet_ranges['E'+ke] = post["auditor"]
    sheet_ranges['G'+ke] = post["observer"]

    wb.save(export_name)
    

    return export_name

# post={}
# post["unit_kerja"]      = "Unit Kerja"
# post["alamat"]          = "Alamat"
# post["surat_tugas"]     = "Surat Tugas"
# post["lead_auditor"]    = "Lead Auditor"
# post["auditor"]         = "Auditor"
# post["auditee"]         = "Auditee"

# exportJadwalRinciInternal(post)
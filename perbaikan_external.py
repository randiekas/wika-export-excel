from openpyxl import load_workbook
def exportPerbaikanExternal(post):
    wb = load_workbook(filename = 'template/perbaikan_external.xlsx')
    export_name = "template/perbaikan_external_edit.xlsx"
    sheet_ranges = wb['Sheet1']
    sheet_ranges['A4'] = post["no_organisasi"]
    sheet_ranges['C4'] = post["no_audit"]
    sheet_ranges['H3'] = post["waktu"]
    
    no = 0
    for item in post["standard"]:
        ke = str(no+4)
        sheet_ranges["D"+ke] = item
    no+=1
    
    for x in post["proses"]:
        sheet_ranges.insert_rows(13)
    no = 0
    for item in post["proses"]:
        ke = str(no+13)
        sheet_ranges["A"+ke] = no+1
        sheet_ranges["B"+ke] = item["kategori"]
        sheet_ranges['C'+ke] = item["clause"]
        sheet_ranges['D'+ke] = item["std"]
        sheet_ranges['E'+ke] = item["auditor"]
        sheet_ranges['F'+ke] = item["description"]
        sheet_ranges['G'+ke] = item["deadline"]
        sheet_ranges['H'+ke] = item["cause"]
        sheet_ranges['I'+ke] = item["evidence"]
        no+=1
    
    sheet_ranges.delete_rows(12)
    wb.save(export_name)
    

    return export_name

# post={}
# post["unit_kerja"]      = "Unit Kerja"
# post["alamat"]          = "Alamat"
# post["surat_tugas"]     = "Surat Tugas"
# post["lead_auditor"]    = "Lead Auditor"
# post["auditor"]         = "Auditor"
# post["auditee"]         = "Auditee"

# exportJadwalRinciInternal(post)